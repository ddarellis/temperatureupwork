/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork.repository;

import org.lavemylogic.temperatureupwork.entities.Measurement;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Panerass
 *
 */
public interface IMeasurementRepository extends MongoRepository<Measurement, String>, IMeasurementRepositoryCustom {

}
