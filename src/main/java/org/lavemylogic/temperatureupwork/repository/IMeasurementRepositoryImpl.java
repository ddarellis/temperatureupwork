/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import com.mongodb.DBObject;

public class IMeasurementRepositoryImpl implements IMeasurementRepositoryCustom {

	/**
	 * Logger instance.
	 */
	private static final Logger logger = LoggerFactory.getLogger(IMeasurementRepositoryImpl.class);

	@Autowired
	MongoTemplate mongoTemplate;

	/**
	 * Calculates and return the average temperature.
	 * 
	 * @return The average temperature.
	 */
	@Override
	public float getAverageTemperature() {
		Double avgTemperature = 0d;
		try {
			Aggregation agg = Aggregation.newAggregation(
					Aggregation.group("ALL").avg("temperature").as("avgTemperature"),
					Aggregation.project("avgTemperature"));

			AggregationResults<DBObject> results = mongoTemplate.aggregate(agg, "measurement", DBObject.class);
			avgTemperature = (Double) results.getMappedResults().get(0).get("avgTemperature");
		} catch (Exception ex) {
			logger.error("ERROR", ex);
		}

		return avgTemperature.floatValue();
	}

}
