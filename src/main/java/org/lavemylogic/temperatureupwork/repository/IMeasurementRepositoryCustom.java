/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork.repository;

/**
 * @author ddarellis
 *
 */
public interface IMeasurementRepositoryCustom {

	/**
	 * Calculates and return the average temperature.
	 * @return The average temperature.
	 */
	public float getAverageTemperature();

}
