/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2016
 */
package org.lavemylogic.temperatureupwork.rest;

import java.security.Principal;

import org.lavemylogic.temperatureupwork.repository.IMeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Panerass
 *
 */
@RestController
@RequestMapping(value = "/rest")
public class MeasurementResource {

	@Autowired
	IMeasurementRepository measurementRepository;

	/**
	 * Returns average temperature.
	 */
	// @PreAuthorize("hasRole('USER')")
	@RequestMapping(value = "/getAverageMeasurementValue", method = RequestMethod.GET)
	public float getAverageTemperature(Principal principal) {
		return measurementRepository.getAverageTemperature();
	}

}
