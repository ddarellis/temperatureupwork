/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork.controller.web;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Panerass
 *
 */
@Controller
public class MeasurementController {
	
	/**
	 * Returns main page.
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping("/")
	public String showHome(Model model, Principal principal) {
		return "main";
	}

}
