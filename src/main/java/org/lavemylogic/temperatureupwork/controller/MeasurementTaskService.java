/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork.controller;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.lavemylogic.temperatureupwork.entities.Measurement;
import org.lavemylogic.temperatureupwork.repository.IMeasurementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author Panerass
 *
 */
@Service
@EnableScheduling
public class MeasurementTaskService {

	/**
	 * Logger instance.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeasurementTaskService.class);

	@Autowired
	IMeasurementRepository measurementRepository;

	/**
	 * Schedule a cron job to run every 10 secs.
	 */
	@Scheduled(cron = "*/10 * * * * *")
	public void doScheduledWork() {
		try {
			int randomTemperature = ThreadLocalRandom.current().nextInt(0, 51);
			measurementRepository.insert(new Measurement(String.valueOf(new Date().getTime()), randomTemperature));
		} catch (Exception ex) {
			logger.error("ERROR", ex);
		}
	}
}
