/* Copyright (C) Darellis Dimitrios - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Darellis Dimitrios <dimidare@gmail.com>, 2017
 */
package org.lavemylogic.temperatureupwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemperatureUpWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemperatureUpWorkApplication.class, args);
	}
}
